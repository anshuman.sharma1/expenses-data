import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import Expenses from "./Expenses";

function App() {
  const [data] = useState([
    { id: 1, name: "Grocery" },
    { id: 2, name: "Shopping" },
    { id: 3, name: "Bills" },
    { id: 4, name: "Recharges" },
  ]);

  return (
    <>
      <h1>Expenses Assignment</h1>

      <Expenses expenseData={data} />
    </>
  );
}

export default App;
