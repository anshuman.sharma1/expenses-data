import React from "react";

const Expenses = ({ expenseData }) => {
  return (
    <div>
      {expenseData?.map((val) => {
        return <p>{val.name}</p>;
      })}
    </div>
  );
};

export default Expenses;
